 -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-abcde-config-file-abcde-config-absent:
  file.absent:
    - name: /home/stooj/.config/abcde/abcde.conf

stooj-dotfiles-personal-workstation-abcde-config-file-abcde-config-dir-absent:
  file.absent:
    - name: /home/stooj/.config/abcde
