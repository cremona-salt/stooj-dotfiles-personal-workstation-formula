# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-abcde-config-file-abcde-config-dir-managed:
  file.directory:
    - name: /home/stooj/.config/abcde
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-abcde-config-file-abcde-config-managed:
  file.managed:
    - name: /home/stooj/.config/abcde/abcde.conf
    - source: {{ files_switch(['abcde.conf.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-abcde-config-file-abcde-conf-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        abcde: {{ stooj_dotfiles_personal_workstation.abcde | json }}
    - require:
      - stooj-dotfiles-personal-workstation-abcde-config-file-abcde-config-dir-managed

stooj-dotfiles-personal-workstation-abcde-config-file-abcde-alias-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/rc_includes/abcde-alias.zsh
    - source: {{ files_switch(['abcde-alias.zsh.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-abcde-config-file-abcde-alias-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        abcde: {{ stooj_dotfiles_personal_workstation.abcde | json }}
