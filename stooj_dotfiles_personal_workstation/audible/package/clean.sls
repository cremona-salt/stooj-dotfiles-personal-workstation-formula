# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}

{%- set installation_dir = stooj_dotfiles_personal_workstation.audible.installation_dir %}
{%- set aaxtomp3_repo = stooj_dotfiles_personal_workstation.audible.aaxtomp3_repo %}
{%- set aaxtomp3_dir = stooj_dotfiles_personal_workstation.audible.aaxtomp3_dir %}
{%- set audible_activator_repo = stooj_dotfiles_personal_workstation.audible.audible_activator_repo %}
{%- set audible_activator_dir = stooj_dotfiles_personal_workstation.audible.audible_activator_dir %}

stooj-dotfiles-personal-workstation-audible-package-install-audible-activator-wrapper-absent:
  file.absent:
    - name: /home/stooj/bin/audible-activator

stooj-dotfiles-personal-workstation-audible-package-install-audible-activator-absent:
  file.absent:
    - name: {{ installation_dir }}/{{ audible_activator_dir }}

stooj-dotfiles-personal-workstation-audible-package-install-aaxtomp3-wrapper-script-absent:
  file.absent:
    - name: /home/stooj/bin/aaxtomp3

stooj-dotfiles-personal-workstation-audible-package-install-aaxtomp3_repo_cloned:
  file.absent:
    - name: {{ installation_dir }}/{{ aaxtomp3_dir }}
