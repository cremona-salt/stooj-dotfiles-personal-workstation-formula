# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set installation_dir = stooj_dotfiles_personal_workstation.audible.installation_dir %}
{%- set aaxtomp3_repo = stooj_dotfiles_personal_workstation.audible.aaxtomp3_repo %}
{%- set aaxtomp3_dir = stooj_dotfiles_personal_workstation.audible.aaxtomp3_dir %}
{%- set audible_activator_repo = stooj_dotfiles_personal_workstation.audible.audible_activator_repo %}
{%- set audible_activator_dir = stooj_dotfiles_personal_workstation.audible.audible_activator_dir %}

stooj-dotfiles-personal-workstation-audible-package-install-src-directory-managed:
  file.directory:
    - name: {{ installation_dir }}
    - user: stooj
    - group: stooj
    - dir_mode: 700

stooj-dotfiles-personal-workstation-audible-package-install-bin-directory-managed:
  file.directory:
    - name: /home/stooj/bin
    - user: stooj
    - group: stooj
    - dir_mode: 755

stooj-dotfiles-personal-workstation-audible-package-install-aaxtomp3-repo-cloned:
  git.latest:
    - name: {{ aaxtomp3_repo }}
    - target: {{ installation_dir }}/{{ aaxtomp3_dir }}
    - user: stooj
    - require:
      - user: stooj
      - stooj-dotfiles-personal-workstation-audible-package-install-src-directory-managed

stooj-dotfiles-personal-workstation-audible-package-install-aaxtomp3-wrapper-script-managed:
  file.managed:
    - name: /home/stooj/bin/aaxtomp3
    - source: {{ files_switch(['aaxtomp3.sh.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-audible-package-install-aaxtomp3-wrapper-script-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        audible: {{ stooj_dotfiles_personal_workstation.audible | json }}
    - require:
      - user: stooj
      - stooj-dotfiles-personal-workstation-audible-package-install-bin-directory-managed

stooj-dotfiles-personal-workstation-audible-package-install-audible-activator-cloned:
  git.latest:
    - name: {{ audible_activator_repo }}
    - target: {{ installation_dir }}/{{ audible_activator_dir }}
    - user: stooj
    - require:
      - user: stooj
      - stooj-dotfiles-personal-workstation-audible-package-install-src-directory-managed

stooj-dotfiles-personal-workstation-audible-package-install-audible-activator-wrapper-managed:
  file.managed:
    - name: /home/stooj/bin/audible-activator
    - source: {{ files_switch(['audible-activator.sh.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-audible-package-install-audible-activator-wrapper-managed',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        audible: {{ stooj_dotfiles_personal_workstation.audible | json }}
    - require:
      - user: stooj
      - stooj-dotfiles-personal-workstation-audible-package-install-bin-directory-managed
