# -*- coding: utf-8 -*-
# vim: ft=sls
---
{%- set optical_drive = salt.grains.get('optical_drive', False ) %}
include:
  - .tor_browser.clean
  - .tiny_media_manager.clean
  - .simple_scan.clean
  - .sane.clean
  - .printing.clean
  - .photography.clean
  - .nextcloud.clean
  - .ncmpcpp.clean
  - .mpd.clean
  - .mpc.clean
  - .lutris.clean
  - .kodi.clean
  - .khard.clean
  - .accounts.clean
  {%- if optical_drive %}
  - .abcde.clean
  {%- endif %}
