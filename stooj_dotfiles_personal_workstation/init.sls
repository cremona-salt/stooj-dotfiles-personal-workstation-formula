# -*- coding: utf-8 -*-
# vim: ft=sls
---
{%- set optical_drive = salt.grains.get('optical_drive', False ) %}
include:
  - stooj_dotfiles_workstation_common
  - .accounts
  - .audible
  - .khard
  - .kodi
  - .lutris
  - .mpc
  - .mpd
  - .ncmpcpp
  - .nextcloud
  - .photography
  - .printing
  - .sane
  - .simple_scan
  - .steam
  - .tiny_media_manager
  - .tor_browser
  {%- if optical_drive %}
  - .abcde
  {%- endif %}
