# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}


stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-config-dir-absent:
  file.absent:
    - name: /home/stooj/.config/vdirsyncer

stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-data-dir-absent:
  file.absent:
    - name: /home/stooj/.local/share/vdirsyncer

stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-status-data-dir-absent:
  file.absent:
    - name: /home/stooj/.local/share/vdirsyncer/status

stooj-dotfiles-personal-workstation-khard-config-file-contacts-dir-absent:
  file.absent:
    - name: /home/stooj/.local/share/contacts

stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-config-file-absent:
  file.absent:
    - name: /home/stooj/.config/vdirsyncer/config

stooj-dotfiles-personal-workstation-khard-config-file-khard-config-dir-absent:
  file.absent:
    - name: /home/stooj/.config/khard

stooj-dotfiles-personal-workstation-khard-config-file-khard-config-file-absent:
  file.absent:
    - name: /home/stooj/.config/khard/khard.conf

stooj-dotfiles-personal-workstation-khard-config-file-khard-mutt-config-file-absent:
  file.absent:
    - name: /home/stooj/.config/mutt/contacts
