# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-config-dir-exists:
  file.directory:
    - name: /home/stooj/.config/vdirsyncer
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-data-dir-exists:
  file.directory:
    - name: /home/stooj/.local/share/vdirsyncer
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-status-data-dir-exists:
  file.directory:
    - name: /home/stooj/.local/share/vdirsyncer/status
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-khard-config-file-contacts-dir-exists:
  file.directory:
    - name: /home/stooj/.local/share/contacts
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-config-file-exists:
  file.managed:
    - name: /home/stooj/.config/vdirsyncer/config
    - source: {{ files_switch(['vdirsyncer.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-config-file-exists',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        khard: {{ stooj_dotfiles_personal_workstation.khard | json }}
    - require:
      - stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-config-dir-exists

stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-start-script-exists:
  file.managed:
    - name: /home/stooj/.config/i3/autostart.d/vdirsyncer
    - source: {{ files_switch(['vdirsyncer.sh.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-khard-config-file-vdirsyncer-start-script-exists',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        khard: {{ stooj_dotfiles_personal_workstation.khard | json }}

stooj-dotfiles-personal-workstation-khard-config-file-khard-config-dir-exists:
  file.directory:
    - name: /home/stooj/.config/khard
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-khard-config-file-khard-config-file-exists:
  file.managed:
    - name: /home/stooj/.config/khard/khard.conf
    - source: {{ files_switch(['khard.conf.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-khard-config-file-khard-config-file-exists',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        khard: {{ stooj_dotfiles_personal_workstation.khard | json }}
    - require:
      - stooj-dotfiles-personal-workstation-khard-config-file-khard-config-dir-exists

stooj-dotfiles-personal-workstation-khard-config-file-khard-mutt-config-file-exists:
  file.managed:
    - name: /home/stooj/.config/mutt/contacts
    - source: {{ files_switch(['mutt-contacts.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-khard-config-file-khard-mutt-config-file-exists',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        khard: {{ stooj_dotfiles_personal_workstation.khard | json }}
