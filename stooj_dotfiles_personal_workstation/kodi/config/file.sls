# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-kodi-config-file-config-dir-exists:
  file.directory:
    - name: /home/stooj/.kodi
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-kodi-config-file-userdata-dir-exists:
  file.directory:
    - name: /home/stooj/.kodi/userdata
    - mode: 700
    - user: stooj
    - group: stooj
    - require:
      - stooj-dotfiles-personal-workstation-kodi-config-file-config-dir-exists

stooj-dotfiles-personal-workstation-kodi-config-file-sources-file-managed:
  file.managed:
    - name: /home/stooj/.kodi/userdata/sources.xml
    - source: {{ files_switch(['sources.xml.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-kodi-config-file-system-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        kodi: {{ stooj_dotfiles_personal_workstation.kodi | json }}
    - require:
      - stooj-dotfiles-personal-workstation-kodi-config-file-userdata-dir-exists
