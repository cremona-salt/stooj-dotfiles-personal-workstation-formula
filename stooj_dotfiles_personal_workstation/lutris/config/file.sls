# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-lutris-config-file-config-dir-exists:
  file.directory:
    - name: /home/stooj/.config/lutris
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-lutris-config-file-system-config-file-managed:
  file.managed:
    - name: /home/stooj/.config/lutris/system.yml
    - source: {{ files_switch(['system.yml.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-lutris-config-file-system-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        lutris: {{ stooj_dotfiles_personal_workstation.lutris | json }}
    - require:
      - stooj-dotfiles-personal-workstation-lutris-config-file-config-dir-exists
