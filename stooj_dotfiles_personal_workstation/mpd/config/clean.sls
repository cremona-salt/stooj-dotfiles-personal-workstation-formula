# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}


stooj-dotfiles-personal-workstation-mpd-config-file-target-parent-dir-absent:
  file.absent:
    - name: /home/stooj/mpd

stooj-dotfiles-personal-workstation-mpd-config-sync-list-absent:
  file.absent:
    - name: /home/stooj/.config/mpd/mpd.conf
