# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-mpd-config-file-config-dir-exists:
  file.directory:
    - name: /home/stooj/.config/mpd
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-mpd-config-file-playlist-dir-exists:
  file.directory:
    - name: /home/stooj/.config/mpd/playlists
    - mode: 700
    - user: stooj
    - group: stooj
    - require:
      - stooj-dotfiles-personal-workstation-mpd-config-file-config-dir-exists

stooj-dotfiles-personal-workstation-mpd-config-file-config-file-managed:
  file.managed:
    - name: /home/stooj/.config/mpd/mpd.conf
    - source: {{ files_switch(['mpd.conf.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-mpd-config-file-system-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        mpd: {{ stooj_dotfiles_personal_workstation.mpd | json }}
    - require:
      - stooj-dotfiles-personal-workstation-mpd-config-file-config-dir-exists

stooj-dotfiles-personal-workstation-mpd-config-file-mpd-start-script-exists:
  file.managed:
    - name: /home/stooj/.config/i3/autostart.d/mpd
    - source: {{ files_switch(['mpd.sh.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-mpd-config-file-mpd-start-script-exists',
                              use_subpath=True
                             )
              }}
    - mode: 700
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        mpd: {{ stooj_dotfiles_personal_workstation.mpd | json }}
