# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}


stooj-dotfiles-personal-workstation-ncmpcpp-config-file-target-parent-dir-absent:
  file.absent:
    - name: /home/stooj/ncmpcpp

stooj-dotfiles-personal-workstation-ncmpcpp-config-sync-list-absent:
  file.absent:
    - name: /home/stooj/.config/ncmpcpp/config
