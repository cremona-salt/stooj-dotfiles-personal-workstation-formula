# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-ncmpcpp-config-file-config-dir-exists:
  file.directory:
    - name: /home/stooj/.config/ncmpcpp
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-ncmpcpp-config-file-config-file-managed:
  file.managed:
    - name: /home/stooj/.config/ncmpcpp/config
    - source: {{ files_switch(['config.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-ncmpcpp-config-file-system-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        ncmpcpp: {{ stooj_dotfiles_personal_workstation.ncmpcpp | json }}
    - require:
      - stooj-dotfiles-personal-workstation-ncmpcpp-config-file-config-dir-exists
