# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}


stooj-dotfiles-personal-workstation-nextcloud-config-file-autostart-file-absent:
  file.absent:
    - name: /home/stooj/.config/autostart/nextcloud.desktop

{%- for account, data in stooj_dotfiles_personal_workstation.nextcloud.accounts.items() %}
stooj-dotfiles-personal-workstation-nextcloud-config-file-{{ account }}-dir-absent:
  file.absent:
    - name: /home/stooj/nextcloud/{{ account }}
{%- endfor %}

stooj-dotfiles-personal-workstation-nextcloud-config-file-target-parent-dir-absent:
  file.absent:
    - name: /home/stooj/nextcloud

stooj-dotfiles-personal-workstation-nextcloud-config-sync-list-absent:
  file.absent:
    - name: /home/stooj/.config/Nextcloud/sync-exclude.lst

stooj-dotfiles-personal-workstation-nextcloud-config-file-config-file-absent:
  file.absent:
    - name: /home/stooj/.config/Nextcloud/nextcloud.cfg

stooj-dotfiles-personal-workstation-nextcloud-config-file-config-dir-absent:
  file.absent:
    - name: /home/stooj/.config/Nextcloud
