# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-nextcloud-config-file-config-dir-exists:
  file.directory:
    - name: /home/stooj/.config/Nextcloud
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-nextcloud-config-file-config-file-managed:
  file.managed:
    - name: /home/stooj/.config/Nextcloud/nextcloud.cfg
    - user: stooj
    - group: stooj
    - replace: False
    - require:
      - stooj-dotfiles-personal-workstation-nextcloud-config-file-config-dir-exists

stooj-dotfiles-personal-workstation-nextcloud-config-sync-list-managed:
  file.managed:
    - name: /home/stooj/.config/Nextcloud/sync-exclude.lst
    - source: {{ files_switch(['sync-exclude.lst.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-nextcloud-config-sync-list-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - context:
        nextcloud: {{ stooj_dotfiles_personal_workstation.nextcloud | json }}
    - require:
      - stooj-dotfiles-personal-workstation-nextcloud-config-file-config-dir-exists

stooj-dotfiles-personal-workstation-nextcloud-config-file-config-file-options-managed:
  ini.options_present:
    - name: /home/stooj/.config/Nextcloud/nextcloud.cfg
    - sections:
        General:
          confirmExternalStorage: true
          newBigFolderSizeLimit: 1000
          optionalDesktopNotifications: true
          optionalServerNotifications: true
          useNewBigFolderSizeLimit: true
        BWLimit:
          useDownloadLimit: -1
          useUploadLimit: -1
    - require:
      - stooj-dotfiles-personal-workstation-nextcloud-config-file-config-file-managed

stooj-dotfiles-personal-workstation-nextcloud-config-file-target-parent-dir-exists:
  file.directory:
    - name: /home/stooj/nextcloud
    - mode: 700
    - user: stooj
    - group: stooj

{%- for account, data in stooj_dotfiles_personal_workstation.nextcloud.accounts.items() %}
stooj-dotfiles-personal-workstation-nextcloud-config-file-{{ account }}-dir-exists:
  file.directory:
    - name: /home/stooj/nextcloud/{{ account }}
    - mode: 700
    - user: stooj
    - group: stooj
    - require:
      - stooj-dotfiles-personal-workstation-nextcloud-config-file-target-parent-dir-exists
{%- endfor %}

stooj-dotfiles-personal-workstation-nextcloud-config-file-autostart-file-exists:
  file.managed:
    - name: /home/stooj/.config/autostart/nextcloud.desktop
    - source: {{ files_switch(['nextcloud.desktop.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-nextcloud-config-file-autostart-file-exists',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - context:
        nextcloud: {{ stooj_dotfiles_personal_workstation.nextcloud | json }}
