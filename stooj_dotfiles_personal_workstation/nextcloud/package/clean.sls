# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}

stooj-dotfiles-personal-workstation-nextcloud-package-clean-package-absent:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles_personal_workstation.nextcloud.pkgs | yaml }}
