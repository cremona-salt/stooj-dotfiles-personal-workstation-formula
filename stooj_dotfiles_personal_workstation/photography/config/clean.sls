 -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{# stooj-dotfiles-personal-workstation-gphoto-config-file-gphoto-config-absent: #}
  {# file.absent: #}
    {# - name: /home/stooj/.config/gphoto/gphoto.conf #}

{# stooj-dotfiles-personal-workstation-gphoto-config-file-gphoto-config-dir-absent: #}
  {# file.absent: #}
    {# - name: /home/stooj/.config/gphoto #}
