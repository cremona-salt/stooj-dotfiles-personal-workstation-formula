# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{# stooj-dotfiles-personal-workstation-gphoto-config-file-gphoto-config-dir-managed: #}
{#   file.directory: #}
{#     - name: /home/stooj/.config/gphoto #}
{#     - mode: 700 #}
{#     - user: stooj #}
{#     - group: stooj #}
{#  #}
{# stooj-dotfiles-personal-workstation-gphoto-config-file-gphoto-config-managed: #}
{#   file.managed: #}
{#     - name: /home/stooj/.config/gphoto/gphoto.conf #}
{#     - source: {{ files_switch(['gphoto.conf.tmpl'], #}
{#                               lookup='stooj-dotfiles-workstation-common-gphoto-config-file-gphoto-conf-managed', #}
{#                               use_subpath=True #}
{#                              ) #}
{#               }} #}
{#     - mode: 600 #}
{#     - user: stooj #}
{#     - group: stooj #}
{#     - template: jinja #}
{#     - context: #}
{#         gphoto: {{ stooj_dotfiles_personal_workstation.gphoto | json }} #}
{#     - require: #}
{#       - stooj-dotfiles-personal-workstation-gphoto-config-file-gphoto-config-dir-managed #}
{#  #}
{# stooj-dotfiles-personal-workstation-gphoto-config-file-gphoto-alias-managed: #}
{#   file.managed: #}
{#     - name: /home/stooj/.config/zsh/rc_includes/gphoto-alias.zsh #}
{#     - source: {{ files_switch(['gphoto-alias.zsh.tmpl'], #}
{#                               lookup='stooj-dotfiles-personal-workstation-gphoto-config-file-gphoto-alias-managed', #}
{#                               use_subpath=True #}
{#                              ) #}
{#               }} #}
{#     - mode: 600 #}
{#     - user: stooj #}
{#     - group: stooj #}
{#     - makedirs: True #}
{#     - template: jinja #}
{#     - context: #}
{#         gphoto: {{ stooj_dotfiles_personal_workstation.gphoto | json }} #}
