# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-printing-config-setup-cups-admin-group-unmanaged:
  file.replace:
    - name: /etc/cups/cups-files.conf
    - pattern: '^SystemGroup .*'
    - repl: SystemGroup sys root wheel
    - append_if_not_found: False
    - ignore_if_missing: True

stooj-dotfiles-personal-workstation-printing-config-file-printing-conf-absent:
  file.absent:
    - name: /home/stooj/.config/printing/printing.conf

stooj-dotfiles-personal-workstation-printing-config-file-printing-dir-absent:
  file.absent:
    - name: /home/stooj/.config/printing
