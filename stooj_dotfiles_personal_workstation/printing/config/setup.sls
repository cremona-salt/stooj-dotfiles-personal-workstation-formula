# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_package_install = tplsubroot ~ '.package.install' %}

include:
  - {{ sls_package_install }}

{%- set ip_addr = stooj_dotfiles_personal_workstation.printing.hp.ip_address %}
{%- set name = stooj_dotfiles_personal_workstation.printing.hp.name %}


stooj-dotfiles-personal-workstation-printing-config-setup-hp-printer-config-dir-managed:
  file.directory:
    - name: /home/stooj/.cups
    - mode: 700
    - user: stooj
    - group: stooj

stooj-dotfiles-personal-workstation-printing-config-setup-hp-printer-config-file-managed:
  file.managed:
    - name: /home/stooj/.cups/lpoptions
    - source: {{ files_switch(['lpoptions.tmpl'],
                              lookup='stooj-dotfiles-personal-workstation-printing-config-setup-hp-printer-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        printing: {{ stooj_dotfiles_personal_workstation.printing | json }}
    - require:
      - stooj-dotfiles-personal-workstation-printing-config-setup-hp-printer-config-dir-managed

stooj-dotfiles-personal-workstation-printing-config-setup-hp-printer-installed:
  cmd.run:
    - name: hp-setup --interactive --auto --type=print --printer={{ name }} -x {{ ip_addr }}
    - creates: /etc/cups/ppd/HP_OfficeJet_Pro_8710.ppd
    - require:
      - sls: {{ sls_package_install }}

stooj-dotfiles-personal-workstation-printing-config-setup-cups-admin-group-managed:
  file.replace:
    - name: /etc/cups/cups-files.conf
    - pattern: '^SystemGroup .*'
    - repl: SystemGroup sys root wheel lp
    - append_if_not_found: False
    - require:
      - sls: {{ sls_package_install }}
