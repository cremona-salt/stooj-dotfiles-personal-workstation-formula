# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- set sls_pacman_conf = 'stooj_dotfiles_workstation_common.pacman.config.file' %}

include:
  - {{ sls_pacman_conf }}

stooj-dotfiles-personal-workstation-steam-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_personal_workstation.steam.pkgs | yaml }}

{%- set gpus = salt.grains.get('gpus', [] ) %}
{%- for gpu in gpus %}
{%- set pkgs = stooj_dotfiles_personal_workstation.steam.drivers.get(gpu.vendor, []) %}
stooj-dotfiles-personal-workstation-steam-package-install-gpu-driver-installed:
  pkg.installed:
    - pkgs: {{ pkgs | yaml }}
    - require:
      - sls: {{ sls_pacman_conf }}
{%- endfor %}
