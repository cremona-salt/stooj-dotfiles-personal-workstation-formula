 -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-tiny-media-manager-config-dir-absent:
  file.absent:
    - name: /home/stooj/.tiny-media-manager/data/

stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-movies-config-absent::
  file.absent:
    - name: /home/stooj/.tiny-media-manager/data/movies.json

stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-tv-shows-config-absent::
  file.absent:
    - name: /home/stooj/.tiny-media-manager/data/tvShows.json
