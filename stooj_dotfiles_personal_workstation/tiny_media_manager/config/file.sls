# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_personal_workstation with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-tiny-media-manager-config-dir-managed:
  file.directory:
    - name: /home/stooj/.tiny-media-manager/data/
    - mode: 700
    - user: stooj
    - group: stooj
    - makedirs: True

stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-movies-config-managed::
  file.managed:
    - name: /home/stooj/.tiny-media-manager/data/movies.json
    - source: {{ files_switch(['movies.json.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-tiny-media-manager-config-file-movies-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        tiny_media_manager: {{ stooj_dotfiles_personal_workstation.tiny_media_manager | json }}
    - require:
      - stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-tiny-media-manager-config-dir-managed

stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-tv-shows-config-managed::
  file.managed:
    - name: /home/stooj/.tiny-media-manager/data/tvShows.json
    - source: {{ files_switch(['tvShows.json.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-tiny-media-manager-config-file-tv-shows-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        tiny_media_manager: {{ stooj_dotfiles_personal_workstation.tiny_media_manager | json }}
    - require:
      - stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-tiny-media-manager-config-dir-managed

stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-tmm-config-managed::
  file.managed:
    - name: /home/stooj/.tiny-media-manager/data/tmm.json
    - source: {{ files_switch(['tmm.json.tmpl'],
                              lookup='stooj-dotfiles-workstation-common-tiny-media-manager-config-file-tmm-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - template: jinja
    - context:
        tiny_media_manager: {{ stooj_dotfiles_personal_workstation.tiny_media_manager | json }}
    - require:
      - stooj-dotfiles-personal-workstation-tiny-media-manager-config-file-tiny-media-manager-config-dir-managed
