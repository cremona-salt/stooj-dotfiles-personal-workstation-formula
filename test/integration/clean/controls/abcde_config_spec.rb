# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation abcde config file' do
  title 'should be absent'

  describe file('/home/stooj/.config/abcde/abcde.conf') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_personal_workstation abcde zsh alias file' do
  title 'should be absent'

  describe file('/home/stooj/.config/zsh/rc_includes/abcde-alias.zsh') do
    it { should_not exist }
  end
end
