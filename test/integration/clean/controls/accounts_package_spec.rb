# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation accounts ledger package' do
  title 'should not be installed'

  describe package('ledger') do
    it { should_not be_installed }
  end
end
