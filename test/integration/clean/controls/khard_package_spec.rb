# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation vdirsyncer package' do
  title 'should not be installed'

  describe package('vdirsyncer') do
    it { should_not be_installed }
  end
end

control 'stooj_dotfiles_personal_workstation vdirsyncer package' do
  title 'should not be installed'

  describe package('vdirsyncer') do
    it { should_not be_installed }
  end
end
