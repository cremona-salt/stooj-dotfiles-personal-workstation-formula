# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation config dir' do
  title 'should be absent'
  describe directory('/home/stooj/.kodi') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_personal_workstation config userdata dir' do
  title 'should be absent'
  describe directory('/home/stooj/.kodi/userdata') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_personal_workstation kodi sources file' do
  title 'should be absent'
  describe file('/home/stooj/.kodi/userdata/sources.xml') do
    it { should_not exist }
  end
end
