# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation kodi package' do
  title 'should not be installed'

  describe package('kodi') do
    it { should_not be_installed }
  end
end
