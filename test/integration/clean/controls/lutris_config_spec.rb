# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation lutris config file' do
  title 'should be absent'
  describe file('/home/stooj/.config/lutris/system.yml') do
    it { should_not exist }
  end
end
