# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation lutris package' do
  title 'should not be installed'

  describe package('lutris') do
    it { should_not be_installed }
  end
end
