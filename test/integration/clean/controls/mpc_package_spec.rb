# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation mpc package' do
  title 'should not be installed'

  describe package('mpc') do
    it { should_not be_installed }
  end
end
