# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation mpd config file' do
  title 'should be absent'
  describe file('/home/stooj/.config/mpd/mpd.conf') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_personal_workstation mpd playlist dir' do
  title 'should be absent'
  describe directory('/home/stooj/.config/mpd/playlists') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_personal_workstation mpd autostart file' do
  title 'should be absent'
  describe file('/home/stooj/.config/i3/autostart.d/mpd') do
    it { should_not exist }
  end
end
