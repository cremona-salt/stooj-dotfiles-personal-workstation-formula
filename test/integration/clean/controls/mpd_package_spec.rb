# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation mpd package' do
  title 'should not be installed'

  describe package('mpd') do
    it { should_not be_installed }
  end
end
