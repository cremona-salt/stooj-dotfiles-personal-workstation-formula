# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation ncmpcpp config file' do
  title 'should be absent'
  describe file('/home/stooj/.config/ncmpcpp/config') do
    it { should_not exist }
  end
end
