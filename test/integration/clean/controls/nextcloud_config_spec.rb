# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation nextcloud config file' do
  title 'should be absent'
  describe file('/home/stooj/.config/Nextcloud/nextcloud.cfg') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_personal_workstation nextcloud example account dir' do
  title 'should be absent'
  describe directory('/home/stooj/nextcloud/example') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_personal_workstation nextcloud sync exclude file' do
  title 'should be absent'

  describe file('/home/stooj/.config/Nextcloud/sync-exclude.lst') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_personal_workstation nextcloud autostart file' do
  title 'should be absent'

  describe file('/home/stooj/.config/autostart/nextcloud.desktop') do
    it { should_not exist }
  end
end
