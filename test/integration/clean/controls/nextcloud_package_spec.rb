# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation nextcloud package' do
  title 'should not be installed'

  describe package('nextcloud-client') do
    it { should_not be_installed }
  end
end

