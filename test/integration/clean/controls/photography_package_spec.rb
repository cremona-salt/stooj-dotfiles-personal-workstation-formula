# frozen_string_literal: true
packages = ['gphoto']

packages.each do |pkg|
  control 'stooj_dotfiles_personal_workstation gphoto ' + pkg + ' package' do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
