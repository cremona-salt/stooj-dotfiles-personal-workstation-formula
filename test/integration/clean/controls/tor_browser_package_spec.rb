# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation tor browser package' do
  title 'should not be installed'

  describe package('torbrowser-launcher') do
    it { should_not be_installed }
  end
end
