# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation abcde config file' do
  title 'should match desired items'

  describe file('/home/stooj/.config/abcde/abcde.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('CDDBMETHOD=musicbrainz') }
  end
end

control 'stooj_dotfiles_personal_workstation abcde zsh alias file' do
  title 'should match desired items'

  describe file('/home/stooj/.config/zsh/rc_includes/abcde-alias.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('alias abcde=\'abcde -c $HOME/.config/abcde/abcde.conf\'') }
  end
end
