# frozen_string_literal: true

packages = [
  'abcde', 'easytag', 'id3v2', 'python-eyed3', 'kid3', 'python-mutagen'
]

packages.each do |pkg|
  control 'stooj_dotfiles_personal_workstation abcde ' + pkg + ' package' do
    title 'should be installed'
    describe package(pkg) do
      it { should be_installed }
    end
  end
end
