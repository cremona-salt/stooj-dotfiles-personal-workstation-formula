# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation accounts ledger package' do
  title 'should be installed'

  describe package('ledger') do
    it { should be_installed }
  end
end
