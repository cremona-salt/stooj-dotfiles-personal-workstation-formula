# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation config dir' do
  title 'should exist'

  describe directory('/home/stooj/.kodi') do

    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_personal_workstation config userdata dir' do
  title 'should exist'

  describe directory('/home/stooj/.kodi/userdata') do

    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_personal_workstation kodi sources file' do
  title 'should match desired items'

  describe file('/home/stooj/.kodi/userdata/sources.xml') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('      <path pathversion="1">http://kodi.emby.media/</path>') }
  end
end
