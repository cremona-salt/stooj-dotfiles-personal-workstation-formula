# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation kodi package' do
  title 'should be installed'

  describe package('kodi') do
    it { should be_installed }
  end
end
