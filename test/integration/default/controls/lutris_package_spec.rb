# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation lutris package' do
  title 'should be installed'

  describe package('lutris') do
    it { should be_installed }
  end
end
