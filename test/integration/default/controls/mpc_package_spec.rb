# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation mpc package' do
  title 'should be installed'

  describe package('mpc') do
    it { should be_installed }
  end
end
