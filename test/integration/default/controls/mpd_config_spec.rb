# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation mpd config file' do
  title 'should match desired items'

  describe file('/home/stooj/.config/mpd/mpd.conf') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('visualizer_fifo_path = /tmp/mpd.fifo') }
  end
end

control 'stooj_dotfiles_personal_workstation playlist dir' do
  title 'should exist'

  describe directory('/home/stooj/.config/mpd/playlists') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_personal_workstation mpd autostart file' do
  title 'should match desired items'

  describe file('/home/stooj/.config/i3/autostart.d/mpd') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('systemctl --user start mpd.service') }
  end
end
