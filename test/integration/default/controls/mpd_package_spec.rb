# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation mpd package' do
  title 'should be installed'

  describe package('mpd') do
    it { should be_installed }
  end
end
