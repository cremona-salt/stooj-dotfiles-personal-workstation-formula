# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation ncmpcpp config file' do
  title 'should match desired items'

  describe file('/home/stooj/.config/ncmpcpp/config') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('visualizer_fifo_path = /tmp/mpd.fifo') }
  end
end
