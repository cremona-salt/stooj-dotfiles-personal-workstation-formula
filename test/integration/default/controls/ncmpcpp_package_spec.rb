# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation ncmpcpp package' do
  title 'should be installed'

  describe package('ncmpcpp') do
    it { should be_installed }
  end
end
