# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation nextcloud config file' do
  title 'should match desired items'

  describe file('/home/stooj/.config/Nextcloud/nextcloud.cfg') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('confirmExternalStorage = true') }
  end
end

control 'stooj_dotfiles_personal_workstation nextcloud example account dir' do
  title 'should be created'

  describe directory('/home/stooj/nextcloud/example') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end

control 'stooj_dotfiles_personal_workstation nextcloud sync exclude file' do
  title 'should match desired items'

  describe file('/home/stooj/.config/Nextcloud/sync-exclude.lst') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('System Volume Information') }
  end
end

control 'stooj_dotfiles_personal_workstation nextcloud autostart file' do
  title 'should match desired items'

  describe file('/home/stooj/.config/autostart/nextcloud.desktop') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('GenericName=File Synchronizer') }
  end
end
