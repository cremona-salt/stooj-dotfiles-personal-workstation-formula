# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation nextcloud package' do
  title 'should be installed'

  describe package('nextcloud-client') do
    it { should be_installed }
  end
end
