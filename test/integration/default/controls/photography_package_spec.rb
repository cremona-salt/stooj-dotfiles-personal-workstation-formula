# frozen_string_literal: true

packages = ['gphoto']

packages.each do |pkg|
  control 'stooj_dotfiles_personal_workstation gphoto ' + pkg + ' package' do
    title 'should be installed'
    describe package(pkg) do
      it { should be_installed }
    end
  end
end

