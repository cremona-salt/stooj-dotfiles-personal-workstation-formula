# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation simple scan package' do
  title 'should be installed'

  describe package('simple-scan') do
    it { should be_installed }
  end
end
