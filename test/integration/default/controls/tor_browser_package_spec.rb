# frozen_string_literal: true

control 'stooj_dotfiles_personal_workstation tor browser package' do
  title 'should be installed'

  describe package('torbrowser-launcher') do
    it { should be_installed }
  end
end
