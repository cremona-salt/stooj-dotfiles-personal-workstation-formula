# -*- coding: utf-8 -*-
# vim: ft=yaml
---
stooj_dotfiles_workstation_common:
  qutebrowser:
    python_script_repo: https://gitlab.com/stooj/qutebrowser-python-scripts.git
  gpg:
    default_key: 6AC6A4C2
    encrypt_to: 6AC6A4C2
